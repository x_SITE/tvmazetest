import React from 'react';
import {Dimensions } from "react-native";

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const icon_search=<FontAwesome5 name={'search'} solid  style={{color:'blue'}}/>
const icon_tv=<FontAwesome5 name={'tv'} solid  style={{color:'blue'}}/>

import HomeView from '../screens/HomeView';
import ShowView from '../screens/ShowView';
const screenWidth = Math.round(Dimensions.get('window').width);
const NaviObj={
  pages:
  {
    'homepage':{
      path:'/HomeView',
      stamID:0,
      screen:HomeView,
      navigationOptions : {
        iconName:'home',
        drawerLabel:'Show Search',
        drawerIcon: ({}) => (
          icon_search
        ),
      }

    },
    'showview':{
      path:'/ShowView',
      stamID:1,
      screen:ShowView,
      tabBarVisible: false,
      navigationOptions : {
        iconName:'search',
        drawerLabel:() => null,
//        drawerIcon: ({}) => (
//          icon_tv
//        ),

      }

    },
  },
  configs:
  {
    drawerWidth: screenWidth*.83,
    edgeWidth: 50-screenWidth,
    initialRouteName:'homepage',
//     unmountInactiveRoutes: true,
//    drawerPosition:'right',
    contentOptions: {
      activeTintColor: '#e91e63',
      itemsContainerStyle: {
      },
      iconContainerStyle: {
      },
      itemStyle: {
        flexDirection: 'row-reverse'
      }
    },

  }
}
export default NaviObj;
