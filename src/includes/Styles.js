import {
  Dimensions,
  StyleSheet,
  Platform,
  StatusBar,
} from 'react-native';
const sWidth = Math.round(Dimensions.get('window').width);
const sHeight = Math.round(Dimensions.get('window').height);

export default StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: '#fff',
      zIndex: 1,
      position: 'relative',

    },
    fullWidthView: {
      width: '100%'
    },
    header: {
      fontSize: 20,
      textAlign: 'center',
      margin: 0,
      backgroundColor: '#ddd',
      width: '100%',
      padding: 5,
      height: 40
    },
    headerFlex: {
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'row',
    },
    mainBLockStyle: {
      width: '100%',
      flex: 1
    },
    mainBLock: {
      backgroundColor: '#ccc',
      marginTop: 10,
      marginBottom: 10,
      width: '100%',
      height: '100%',
      paddingTop: 0,
      paddingRight: 10,
      paddingBottom: 100,
      paddingLeft: 10,
    },
    mainViewData: {
      textAlign: 'center',
      color: '#000',
      marginBottom: 5,
    },
    showMainBlock: {
      flex: 1,
      width: '100%',
      alignItems: 'center',
      zIndex: 1,
    },
    showLink: {
      alignItems: 'center',
    },
    showImg: {
      marginTop: 10,
      marginBottom: 10,
      width: 210,
      height: 295,
    },
    resLink: {
      marginBottom: 1,
      padding: 1,
      fontSize: 16,
      borderBottomColor: '#ddd',
      borderBottomWidth: 1,
    },
    searchInputView: {
      flex: 1,
      flexDirection: 'row',
      textAlign: 'center',
      justifyContent: 'center',
    },
    searchInput: {
      height: 30,
      borderColor: '#fff',
      width: '100%',
      borderWidth: 1,
      fontSize: 20,
      padding: 2
    },
    searchResRow: {
      backgroundColor: '#fff',
      width: '100%',
      position: 'relative',
      marginTop: 30
    },
    basicShowData: {
      textAlign: 'center',
      width: '100%',
      justifyContent: 'center',
    },
    centerText:{
    textAlign:'center',
    },
    showTitleMain: {
      flex: 1,
      flexDirection: 'row',
      textAlign: 'center',
      width: '100%',
      justifyContent: 'center',
      position: 'absolute'
    },
    showTitle: {
      fontSize: 16,
      fontWeight: '900',
      color: '#000',
      textAlign: 'center',

    },
    showRating: {
      marginBottom: 15,
      textAlign: 'center',
      justifyContent: 'center'
    },
    showRatingStar: {
      color: '#ffe54e',
      fontSize: 24,
    },
    menuBars: {
      color: '#333',
      fontSize: 31,
    },
    showSummery: {
      fontSize: 16,
      color: '#222'
    },
    genersRow: {
      fontSize: 16,
      fontWeight: '800',
      color: '#666'
    }
  }

);