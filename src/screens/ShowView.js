import React from 'react';
import {View,Text,ScrollView,TouchableOpacity,Image} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AppHeader from '../components/AppHeader';
//image and rating
import ShowImage from '../components/ShowImage';
import AppFooter from '../components/AppFooter';

import styles from '../includes/Styles';
import urls from '../includes/Urls';
const icon_stars = <FontAwesome5 name={'star'} solid  style={styles.showRatingStar}/>;

export default class HomeView extends React.Component{
  constructor(props) {
    super(props)
    this.state={
      showID:this.props.navigation.getParam('showID', 5),
      currentShow:this.props.navigation.getParam('showBasics', {})
    }
  }
  getTheBolded(text) {
    text = text.split(/(<.*?>.*?<\/.*?>)/g);
    for(var i = 1; i < text.length; i += 2) {
      var word = text[i].replace(/<.*?>(.*?)<\/.*?>/, '$1');
      text[i] = <Text style={{fontWeight:'900'}} key={'sumTxt'+i}>{word}</Text>;
    }
    return text;
  }
  basicHeaders(){
    const headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept','application/json');
    return headers;
  }
  gotNewShow(showObj){
    this.setState({
      showFetched:true,
      currentShow:showObj,
    })
  }
  renderGenres(arr){
    var txt='';
    for(var i=0;i<arr.length;i++){
      txt+=((txt!='')?', ':'')+arr[i];
    }
    return <Text style={styles.genersRow}>{txt}.</Text>
  }
  renderShow(){
    let o=this.state.currentShow;
    let cleanSumaary= o.summary.replace(/(<\/p?>)|(<p?>)/ig,'');
    return <View style={styles.showMainBlock}>
    <ShowImage tmpImgStyle={styles.showImg} showIMG={o.image.medium} showRating={o.rating.average} />
    <View>
    <Text style={styles.showSummery}>
    {this.getTheBolded(cleanSumaary)}
    </Text>
    {this.renderGenres(o.genres)}
    </View>
    </View>
  }
  componentDidMount(){
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.navigation.state.params.showID) {
      this.setState({
        showID:nextProps.navigation.state.params.showID,
        currentShow:nextProps.navigation.state.params.showBasics
      })
    }
  }
  render(){
    return(
      <View style={styles.container}>
      <AppHeader navigation={this.props.navigation} showSearchIcon={false} showName={this.state.currentShow.name}/>

      <ScrollView contentContainerStyle={styles.mainBLock} style={styles.mainBLockStyle}>
      <TouchableOpacity
      onPress={() => {this.props.navigation.navigate('homepage',{wentBack:true})}}
      >
      <Text>Back</Text>
      </TouchableOpacity>
      {(this.state.showID>0 && Object.entries(this.state.currentShow).length > 0) ?
        this.renderShow()
        :null
      }
      </ScrollView>
      <AppFooter  />
      </View>
    )
  }
}
