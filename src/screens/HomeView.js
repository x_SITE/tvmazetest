import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image
} from 'react-native';

import AppHeader from '../components/AppHeader';
//image, title & rating
import ShowImage from '../components/ShowImage';
import AppFooter from '../components/AppFooter';
import styles from '../includes/Styles';
import urls from '../includes/Urls';

export default class HomeView extends React.Component{
  constructor(props) {
    super(props)
    this.state={
      showID:0,
      showFetched:false,
      wentBack:false,
      currentShow:{
        id:0,
        image:{
          medium:'https://static.tvmaze.com/images/tvm-header-logo.png'
        },
        rating:{
          average:10
        },
        name:'tvMaze API Lookup',

      }
    }
    this.gotNewShow=this.gotNewShow.bind(this);
  }

  basicHeaders(){
    const headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept','application/json');
    return headers;
  }
  gotNewShow(showObj){

    this.setState({
      showFetched:true,
      currentShow:showObj,
      wentBack:false
    })
  }
  renderShow(){
    let o=this.state.currentShow;
    let tmpImgStyle=this.state.showFetched?styles.showImg:[styles.showImg,{height:50,width:158}]
    return <View style={styles.showMainBlock}>
    <TouchableOpacity onPress={() => {this.props.navigation.navigate('showview',{showID:o.id,showBasics: o})}} style={styles.showLink} >
    <ShowImage showName={o.name} tmpImgStyle={tmpImgStyle} showIMG={o.image.medium} showRating={o.rating.average} />
    </TouchableOpacity>
    </View>
  }
  fetchMyShow(){
    fetch(urls.singleShow+this.state.showID,{
      method:'get',
      headers:this.basicHeaders(),
    })
    .then(response=>response.json())
    .then(
      responseJson=>this.gotNewShow(responseJson)

    )
    .catch(
      err=>alert('we had problem fetching the show info: '+err)
    );
  }
  componentDidMount(){
  }
  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.navigation.state.params!='undefined' && nextProps.navigation.state.params.wentBack) {
      this.setState({
        wentBack:true
      })

    }

  }
  render(){
    return(
      <View style={styles.container}>
      <AppHeader navigation={this.props.navigation} showSearchIcon={true} gotNewShow={this.gotNewShow} wentBack={this.state.wentBack}/>
      <ScrollView contentContainerStyle={styles.mainBLock} style={styles.mainBLockStyle}>
      {this.renderShow()}
      </ScrollView>
      <AppFooter />
      </View>
    )
  }
}
