import React from 'react';
import {
	View,
	Image,
    Text,
} from 'react-native';
import styles from '../includes/Styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const icon_stars = <FontAwesome5 name={'star'} solid  style={styles.showRatingStar}/>;
export default class ShowImage extends React.Component {
  constructor(props) {
    super(props)
   }
  render(){
    return (
    <View>
        <Image style={this.props.tmpImgStyle} source={{uri: this.props.showIMG}} />
        {
        (this.props.showName && this.props.showName!='')?
        <Text style={styles.showTitle}>{this.props.showName}</Text>
        :null
        }
	    <View style={styles.showRating}>
	    <Text style={styles.centerText}>{icon_stars} {this.props.showRating}</Text>
	    </View>
    </View>

    );
  }
}
