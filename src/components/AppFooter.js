import React from 'react';
import {
	View,
  Text,
} from 'react-native';
import styles from '../includes/Styles';

export default class AppFooter extends React.Component {
  constructor(props) {
    super(props)
   }
  render(){
    return (
		<View style={styles.fullWidthView}>
       	    <Text style={styles.header}>
    	        I'm a Dumb Footer :(
    	    </Text>
    	</View>
    );
  }
}
