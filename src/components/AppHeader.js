import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
  Keyboard,
} from 'react-native';
import styles from '../includes/Styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import urls from '../includes/Urls';
import { StackActions, NavigationActions } from 'react-navigation';
//const icon_menu = <FontAwesome5 name={'bars'} solid  style={styles.menuBars}/>;
const icon_search = <FontAwesome5 name={'search'} solid  style={styles.menuBars}/>;
const icon_tv=<FontAwesome5 name={'tv'} solid  style={styles.menuBars}/>
export default class AppHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state={
      showSearch:false,
      showSearchIcon:this.props.showSearchIcon,
      showName:this.props.showName?this.props.showName:'',
      searchText:'',
      gotSearches:false,
      searchHeight:40,
      showList:[],
    }
  }

  basicHeaders(){
    const headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept','application/json');
    return headers;
  }
  goAndReset(obj){
    this.props.navigation.navigate('showview',{showID:obj.id,showBasics: obj})
  }
//	this.props.navigation.navigate('showview',{showID:o.id,showBasics: o})
  renderSearchRes(){
    var tmpArr=this.state.showList;
    var arrBack=[];
    for(var i=0; i<tmpArr.length;i++){
      let o=tmpArr[i].show;
      arrBack.push (
        <View key={'sRes'+i}>
        <TouchableOpacity onPress={() => {
          this.props.gotNewShow(o)
          this.refs.searchInput.blur()
          Keyboard.dismiss
        }
      }
      accessible={false}
      style={ styles.resLink}
      >
      <Text >{o.name}</Text>
      </TouchableOpacity>
      </View>
	    );
	  }
	  return arrBack;
	}

	foundSearches(showArr){
	  if( showArr.length > 0 ){

	    this.setState({
	      gotSearches:true,
	      searchHeight:(showArr.length*26+40),
	      showList:showArr,
	    })
	  }
	  else{
	    this.setState({
	      gotSearches:false,
	      searchHeight:40,
	      showList:[]
	    })
	  }
	}

	searchForShows(){

	  if(this.state.searchText.length>1){
	    fetch(urls.search+this.state.searchText,{
	      method:'get',
	      headers:this.basicHeaders(),
	    })
	    .then(response=>response.json())
	    .then(
	      responseJson=>this.foundSearches(responseJson)
	    )
	    .catch(
	      err=>alert('we had problem fetching the show info: '+err)
	    );

	  }
	  else{
	    this.setState({
	      gotSearches:false
	    })
	  }
	}

	openSearch(){
	  let tmpH=!this.state.showSearch?(this.state.showList.length*26+40):40;
	  this.setState({
	    showSearch:!this.state.showSearch,
	    searchText:'',
	    searchHeight:tmpH,
	  })
	}
	componentWillReceiveProps(nextProps) {
	  if (typeof nextProps.navigation.state.params!='undefined' && nextProps.navigation.state.params.wentBack) {
	    this.refs.searchInput.focus()
	    this.setState({
	      showSearch:true,
	    })
	  }
	}
//	optional menu
//	 <TouchableOpacity onPress={() => {this.props.navigation.openDrawer();} }>
//      <Text>{icon_menu}</Text>
//   </TouchableOpacity>

  render(){
	  return (
	   <View  style={[styles.header,{height:this.state.searchHeight}]}>
	    <View style={styles.headerFlex}>
  	    {this.state.showSearchIcon?
          <TouchableOpacity onPress={() => {this.openSearch()} }>
    		    <Text>{icon_search}</Text>
          </TouchableOpacity>
          :
          <Text>{icon_tv}</Text>
        }
  		  {this.state.showSearch?
          <View>
  		      <View style={styles.searchInputView}>
  		        <TextInput
                style={styles.searchInput}
                ref={'searchInput'}
  				      onChangeText={(searchText) => {
  				        this.setState({searchText})
  				        this.searchForShows(searchText)
  				      }
  				    }
  				    value={this.state.searchText}
  				    placeholder='search'
  				    autoFocus={true}
  			      />
            </View>
  		      <View style={styles.searchResRow}>
  		        {this.state.gotSearches?
  		          this.renderSearchRes()
  		          :null
  		        }
  		      </View>
  		    </View>
  		    :null
  		  }
  		  {this.state.showName!=''?
  		    <View  style={styles.showTitleMain}>
  		      <View>
  		        <Text style={styles.showTitle}>{this.props.showName}</Text>
  		      </View>
  		   </View>
  		  :null
  		  }
	     </View>
	   </View>
    );
   }
}
