import React from 'react';
import {StackActions,createDrawerNavigator,NavigationActions,createAppContainer} from 'react-navigation';
import NaviObj from './src/includes/NaviObj';
import HomeView from './src/screens/HomeView';

const MyDrawerNavigator = createDrawerNavigator(NaviObj.pages,NaviObj.configs)
const  MyMenuApp= createAppContainer(MyDrawerNavigator)
export default MyMenuApp;
